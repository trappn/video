import psutil, time, subprocess    

while True:
    if "BIS.exe" in (p.name() for p in psutil.process_iter()):
        # BIS is running, check if video is running:
        if "video.exe" in (p.name() for p in psutil.process_iter()):
            # video already running, do nothing
            pass
        else:
            subprocess.call(r"C:\Program Files\BrukerAXS\Video_NT\video.exe")
    else:
        # BIS is NOT running, do nothing
        pass
    time.sleep(10)

