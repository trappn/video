import sys
from math import atan2, cos, sin
from PyQt5 import QtCore as qtc
from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtMultimedia as qtmm
from PyQt5 import QtMultimediaWidgets as qtmmw

class ColorButton(qtw.QPushButton):

    changed = qtc.pyqtSignal()

    def __init__(self, default_color, changed=None):
        super().__init__()
        self.setFixedSize(qtc.QSize(100,25))
        self.set_color(qtg.QColor(default_color))
        self.clicked.connect(self.on_click)
        if changed:
            self.changed.connect(changed)

    def set_color(self, color):
        self._color = color
        # update icon
        pixmap = qtg.QPixmap(96, 19)
        pixmap.fill(self._color)
        self.setIcon(qtg.QIcon(pixmap))
        self.setIconSize(qtc.QSize(96,19))

    def on_click(self):
        color = qtw.QColorDialog.getColor(self._color)
        if color:
            self.set_color(color)
            self.changed.emit()

            
class SettingsWindow(qtw.QWidget):             

    applied = qtc.pyqtSignal()

    def __init__(self, parent, applied=None):                    
        super().__init__()
        # handle exit condition:
        self.parent = parent
        parent.destroyed.connect(self.close)
        # recover settings:
        if applied:
            self.applied.connect(applied)
        self.setGeometry(self.parent.geometry().adjusted(8, 50, 0, 0))       
        self.setFixedSize(qtc.QSize(680,445))
        self.setWindowTitle('VIDEO.EXE Settings')
        self.setWindowFlags(qtc.Qt.WindowStaysOnTopHint)
        buttonSize = qtc.QSize(110,35)
        layout = qtw.QVBoxLayout()
        self.setLayout(layout)
        # settings:
        self.settingsbox = qtw.QWidget()
        settingslayout_top = qtw.QFormLayout()
        settingslayout_bottom = qtw.QGridLayout()
        self.settingsbox.setLayout(qtw.QVBoxLayout())
        self.camselect = qtw.QComboBox()
        self.camselect.setMinimumWidth(263)
        self.camselect.setMaximumWidth(263)
        self.camselect.addItems([str(i+1)+':'+c.description() for i,c in enumerate(parent.available_cameras)])
        self.color1 = ColorButton((qtg.QColor('white')))
        self.color2 = ColorButton((qtg.QColor('white')))
        self.wsize1 = qtw.QRadioButton("cam native")
        self.wsize2 = qtw.QRadioButton("current")
        self.aot = qtw.QCheckBox()
        self.showdot = qtw.QCheckBox()
        self.showcross = qtw.QCheckBox()
        self.showticks = qtw.QCheckBox()
        self.showticktxt = qtw.QCheckBox()
        self.showcircle1 = qtw.QCheckBox()
        self.showcircle2 = qtw.QCheckBox()
        self.showcircle3 = qtw.QCheckBox()
        self.showcircle1txt = qtw.QCheckBox()
        self.showcircle2txt = qtw.QCheckBox()
        self.showcircle3txt = qtw.QCheckBox()
        self.showcamctr = qtw.QCheckBox()
        self.offx = qtw.QSpinBox()
        self.offx.setRange(-int(self.parent.camwidth/2), int(self.parent.camwidth/2))
        self.offx.setSingleStep(1)
        self.offx.setSuffix('pix')
        self.offx.setAccelerated(True)
        self.offx.setMaximumWidth(100)        
        self.offx.setValue(0)
        self.offy = qtw.QSpinBox()
        self.offy.setRange(-int(self.parent.camheight/2), int(self.parent.camheight/2))
        self.offy.setSingleStep(1)
        self.offy.setSuffix('pix')
        self.offy.setAccelerated(True)  
        self.offy.setMaximumWidth(100)        
        self.offy.setValue(0)
        self.tickspacing = qtw.QSpinBox()
        self.tickspacing.setRange(0, int(self.parent.camheight/2))
        self.tickspacing.setSingleStep(1)
        self.tickspacing.setSuffix('pix')
        self.tickspacing.setAccelerated(True)  
        self.tickspacing.setMaximumWidth(100)        
        self.tickspacing.setValue(25)
        self.tickscale = qtw.QDoubleSpinBox()
        self.tickscale.setRange(0, 10)
        self.tickscale.setSingleStep(0.01)
        self.tickscale.setSuffix('mm')
        self.tickscale.setAccelerated(True)  
        self.tickscale.setMaximumWidth(100)        
        self.tickscale.setValue(0.05)
        self.circle1spacing = qtw.QSpinBox()
        self.circle1spacing.setRange(0, int(self.parent.camheight/2))
        self.circle1spacing.setSingleStep(1)
        self.circle1spacing.setSuffix('pix')
        self.circle1spacing.setAccelerated(True)  
        self.circle1spacing.setMaximumWidth(100)        
        self.circle1spacing.setValue(50)
        self.circle1scale = qtw.QDoubleSpinBox()
        self.circle1scale.setRange(0, 10)
        self.circle1scale.setSingleStep(0.01)
        self.circle1scale.setSuffix('mm')
        self.circle1scale.setAccelerated(True)  
        self.circle1scale.setMaximumWidth(100)        
        self.circle1scale.setValue(0.1)
        self.circle2spacing = qtw.QSpinBox()
        self.circle2spacing.setRange(0, int(self.parent.camheight/2))
        self.circle2spacing.setSingleStep(1)
        self.circle2spacing.setSuffix('pix')
        self.circle2spacing.setAccelerated(True)  
        self.circle2spacing.setMaximumWidth(100)        
        self.circle2spacing.setValue(100)
        self.circle2scale = qtw.QDoubleSpinBox()
        self.circle2scale.setRange(0, 10)
        self.circle2scale.setSingleStep(0.01)
        self.circle2scale.setSuffix('mm')
        self.circle2scale.setAccelerated(True)  
        self.circle2scale.setMaximumWidth(100)        
        self.circle2scale.setValue(0.2)
        self.circle3spacing = qtw.QSpinBox()
        self.circle3spacing.setRange(0, int(self.parent.camheight/2))
        self.circle3spacing.setSingleStep(1)
        self.circle3spacing.setSuffix('pix')
        self.circle3spacing.setAccelerated(True)  
        self.circle3spacing.setMaximumWidth(100)        
        self.circle3spacing.setValue(150)
        self.circle3scale = qtw.QDoubleSpinBox()
        self.circle3scale.setRange(0, 10)
        self.circle3scale.setSingleStep(0.01)
        self.circle3scale.setSuffix('mm')
        self.circle3scale.setAccelerated(True)  
        self.circle3scale.setMaximumWidth(100)        
        self.circle3scale.setValue(0.3)
        self.measpix = qtw.QSpinBox()
        self.measpix.setRange(0, int(self.parent.camheight/2))
        self.measpix.setSingleStep(1)
        self.measpix.setSuffix('pix')
        self.measpix.setAccelerated(True)  
        self.measpix.setMaximumWidth(100)        
        self.measpix.setValue(200)
        self.measmm = qtw.QDoubleSpinBox()
        self.measmm.setRange(0, 10)
        self.measmm.setSingleStep(0.01)
        self.measmm.setSuffix('mm')
        self.measmm.setAccelerated(True)  
        self.measmm.setMaximumWidth(100)        
        self.measmm.setValue(0.4)
        self.penpix = qtw.QSpinBox()
        self.penpix.setRange(1,14)
        self.penpix.setSingleStep(1)
        self.penpix.setSuffix('pix')
        self.penpix.setAccelerated(False)  
        self.penpix.setMaximumWidth(100)        
        self.penpix.setValue(2)        
        settingslayout_top.addRow('Camera: ', self.camselect)
        settingslayout_bottom.addWidget(qtw.QLabel('Startup Size: '),0,0)
        settingslayout_bottom.addWidget(self.wsize1,0,1,1,2)
        settingslayout_bottom.addWidget(self.wsize2,0,3)
        settingslayout_bottom.addWidget(qtw.QLabel('Always on Top: '),0,4,1,3)
        settingslayout_bottom.addWidget(self.aot,0,6)
        settingslayout_bottom.addWidget(qtw.QLabel('Color (Circles/Crosshair/Labels): '),1,0,1,3)
        settingslayout_bottom.addWidget(self.color1,1,3)
        settingslayout_bottom.addWidget(qtw.QLabel('Color (Markers): '),1,4)
        settingslayout_bottom.addWidget(self.color2,1,6)
        settingslayout_bottom.addWidget(qtw.QLabel('Xtal Ctr Dot: '),2,0)
        settingslayout_bottom.addWidget(self.showdot,2,1)
        settingslayout_bottom.addWidget(qtw.QLabel('Cam Ctr: '), 2,4)
        settingslayout_bottom.addWidget(self.showcamctr,2,5) 
        settingslayout_bottom.addWidget(qtw.QLabel('Xtal Crosshair: '),3,0)
        settingslayout_bottom.addWidget(self.showcross,3,1)
        settingslayout_bottom.addWidget(qtw.QLabel('Line Thickness: '),3,4)
        settingslayout_bottom.addWidget(self.penpix,3,6)
        settingslayout_bottom.addWidget(qtw.QLabel('Ticks: '),4,0)
        settingslayout_bottom.addWidget(self.showticks,4,1)
        settingslayout_bottom.addWidget(qtw.QLabel('Spacing: '),4,2)
        settingslayout_bottom.addWidget(self.tickspacing,4,3)
        settingslayout_bottom.addWidget(qtw.QLabel('             Scale: '),4,4)
        settingslayout_bottom.addWidget(self.showticktxt,4,5)
        settingslayout_bottom.addWidget(self.tickscale,4,6)
        settingslayout_bottom.addWidget(qtw.QLabel('Xtal Ctr Circle 1: '),5,0)
        settingslayout_bottom.addWidget(self.showcircle1,5,1)
        settingslayout_bottom.addWidget(qtw.QLabel('Spacing: '),5,2)
        settingslayout_bottom.addWidget(self.circle1spacing,5,3)
        settingslayout_bottom.addWidget(qtw.QLabel('             Scale: '),5,4)
        settingslayout_bottom.addWidget(self.showcircle1txt,5,5)
        settingslayout_bottom.addWidget(self.circle1scale,5,6)
        settingslayout_bottom.addWidget(qtw.QLabel('Xtal Ctr Circle 2: '),6,0)
        settingslayout_bottom.addWidget(self.showcircle2,6,1)
        settingslayout_bottom.addWidget(qtw.QLabel('Spacing: '),6,2)
        settingslayout_bottom.addWidget(self.circle2spacing,6,3)
        settingslayout_bottom.addWidget(qtw.QLabel('             Scale: '),6,4)
        settingslayout_bottom.addWidget(self.showcircle2txt,6,5)
        settingslayout_bottom.addWidget(self.circle2scale,6,6)
        settingslayout_bottom.addWidget(qtw.QLabel('Xtal Ctr Circle 3: '),7,0)
        settingslayout_bottom.addWidget(self.showcircle3,7,1)
        settingslayout_bottom.addWidget(qtw.QLabel('Spacing: '),7,2)
        settingslayout_bottom.addWidget(self.circle3spacing,7,3)
        settingslayout_bottom.addWidget(qtw.QLabel('             Scale: '),7,4)
        settingslayout_bottom.addWidget(self.showcircle3txt,7,5)
        settingslayout_bottom.addWidget(self.circle3scale,7,6)
        settingslayout_bottom.addWidget(qtw.QLabel('Ref. Object (for measurements): '),8,0,1,3)
        settingslayout_bottom.addWidget(self.measpix,8,3)
        settingslayout_bottom.addWidget(qtw.QLabel('             == '),8,4)
        settingslayout_bottom.addWidget(self.measmm,8,6)
        settingslayout_bottom.addWidget(qtw.QLabel('Xtal Ctr Offsets: '),9,0)
        settingslayout_bottom.addWidget(qtw.QLabel('         X: '),9,2)
        settingslayout_bottom.addWidget(self.offx,9,3)
        settingslayout_bottom.addWidget(qtw.QLabel('    Y: '),9,5)         
        settingslayout_bottom.addWidget(self.offy,9,6)
        self.settingsbox.layout().addLayout(settingslayout_top)
        self.settingsbox.layout().addLayout(settingslayout_bottom)
        # bottom button box:
        self.buttonbox = qtw.QWidget()
        self.buttonbox.setFixedHeight(60)
        self.cancelButton = qtw.QPushButton('Cancel', clicked=self.on_close)
        self.applyButton = qtw.QPushButton('OK', clicked=self.on_apply)
        self.applyButton.setDefault(True)
        self.cancelButton.setFixedSize(buttonSize)
        self.applyButton.setFixedSize(buttonSize)
        buttonlayout = qtw.QHBoxLayout()
        self.buttonbox.setLayout(buttonlayout)
        buttonlayout.setAlignment(qtc.Qt.AlignRight)
        buttonlayout.addWidget(self.cancelButton)
        buttonlayout.addWidget(self.applyButton)
        # add widgets to layout:
        layout.addWidget(self.settingsbox)
        layout.addStretch()
        layout.addWidget(self.buttonbox)
        # get settings and end main UI code
        self.get_settings()
        # connect signals:
        self.camselect.currentIndexChanged.connect(parent.select_camera)
        self.color1.changed.connect(parent.update_overlay)
        self.color2.changed.connect(parent.update_overlay)
        self.showdot.stateChanged.connect(parent.update_overlay)
        self.showcross.stateChanged.connect(parent.update_overlay)
        self.showticks.stateChanged.connect(parent.update_overlay)
        self.showcircle1.stateChanged.connect(parent.update_overlay)
        self.showcircle2.stateChanged.connect(parent.update_overlay)
        self.showcircle3.stateChanged.connect(parent.update_overlay)
        self.showticktxt.stateChanged.connect(parent.update_overlay)
        self.showcircle1txt.stateChanged.connect(parent.update_overlay)
        self.showcircle2txt.stateChanged.connect(parent.update_overlay)
        self.showcircle3txt.stateChanged.connect(parent.update_overlay)
        self.showcamctr.stateChanged.connect(parent.update_overlay)
        self.offx.textChanged.connect(parent.update_overlay)
        self.offy.textChanged.connect(parent.update_overlay)
        self.tickspacing.textChanged.connect(parent.update_overlay)
        self.tickscale.textChanged.connect(parent.update_overlay)
        self.circle1spacing.textChanged.connect(parent.update_overlay)
        self.circle1scale.textChanged.connect(parent.update_overlay)
        self.circle2spacing.textChanged.connect(parent.update_overlay)
        self.circle2scale.textChanged.connect(parent.update_overlay)
        self.circle3spacing.textChanged.connect(parent.update_overlay)
        self.circle3scale.textChanged.connect(parent.update_overlay)
        self.penpix.textChanged.connect(parent.update_overlay)
        self.measmm.textChanged.connect(parent.update_overlay)
        self.measpix.textChanged.connect(parent.update_overlay)
        self.aot.stateChanged.connect(parent.update_window_properties)
        self.show()
        
    def keyPressEvent(self, qKeyEvent):
        if qKeyEvent.key() == qtc.Qt.Key_Return: 
            self.on_apply()
        else:
            super().keyPressEvent(qKeyEvent)  

    def on_apply(self):
        self.save_settings()
        self.applied.emit()
        self.close()

    def save_settings(self):
        # store camera&port as unique device string:
        try:
            camera = self.parent.available_cameras[self.camselect.currentIndex()].deviceName()
            self.parent.settings.setValue('camera', camera)
        except:
            pass
        self.parent.settings.setValue('color1', self.color1._color)
        self.parent.settings.setValue('color2', self.color2._color)
        self.parent.settings.setValue('alwaysontop', self.aot.isChecked())
        self.parent.settings.setValue('pen_pixels', self.penpix.value())
        self.parent.settings.setValue('show_center', self.showcamctr.isChecked())
        self.parent.settings.setValue('show_dot', self.showdot.isChecked())
        self.parent.settings.setValue('show_crosshair', self.showcross.isChecked())
        self.parent.settings.setValue('show_ticks', self.showticks.isChecked())
        self.parent.settings.setValue('show_circle1', self.showcircle1.isChecked())
        self.parent.settings.setValue('show_circle2', self.showcircle2.isChecked())
        self.parent.settings.setValue('show_circle3', self.showcircle3.isChecked())
        self.parent.settings.setValue('show_ticktxt', self.showticktxt.isChecked())
        self.parent.settings.setValue('show_circle1txt', self.showcircle1txt.isChecked())
        self.parent.settings.setValue('show_circle2txt', self.showcircle2txt.isChecked())
        self.parent.settings.setValue('show_circle3txt', self.showcircle3txt.isChecked())
        self.parent.settings.setValue('center_x', self.offx.value())
        self.parent.settings.setValue('center_y', self.offy.value())
        self.parent.settings.setValue('tick_pixels', self.tickspacing.value())
        self.parent.settings.setValue('tick_distmm', self.tickscale.value())
        self.parent.settings.setValue('rad_circle1', self.circle1spacing.value())
        self.parent.settings.setValue('rad_circle1mm', self.circle1scale.value())
        self.parent.settings.setValue('rad_circle2', self.circle2spacing.value())
        self.parent.settings.setValue('rad_circle2mm', self.circle2scale.value())
        self.parent.settings.setValue('rad_circle3', self.circle3spacing.value())
        self.parent.settings.setValue('rad_circle3mm', self.circle3scale.value())
        self.parent.settings.setValue('measurements_pix', self.measpix.value())
        self.parent.settings.setValue('measurements_mm', self.measmm.value())
        if self.wsize2.isChecked():
            self.parent.settings.setValue('custom_geometry', self.parent.geometry())
            self.parent.settings.setValue('use_custom_geometry', 1)
        else:
            self.parent.settings.setValue('use_custom_geometry', 0)

    def get_settings(self):
        # retranslate unique camera&port strings back to selection index:
        for i, c in enumerate(self.parent.available_cameras):
            if self.parent.settings.value('camera', 'none') == c.deviceName():
                self.camselect.setCurrentIndex(i)
                self.parent.select_camera(i)
        self.color1.set_color(self.parent.settings.value('color1', qtg.QColor('#FFFFFF')))
        self.color2.set_color(self.parent.settings.value('color2', qtg.QColor('#FFFF00')))
        if self.parent.settings.value('use_custom_geometry', False, type=bool) == True:
            self.wsize1.setChecked(False)
            self.wsize2.setChecked(True)
        else:
            self.wsize1.setChecked(True)
            self.wsize2.setChecked(False)
        self.aot.setChecked(self.parent.settings.value('alwaysontop', False, type=bool))
        self.penpix.setValue(int(self.parent.settings.value('pen_pixels', 2)))
        self.showcamctr.setChecked(self.parent.settings.value('show_center', False, type=bool))
        self.showdot.setChecked(self.parent.settings.value('show_dot', False, type=bool))
        self.showcross.setChecked(self.parent.settings.value('show_crosshair', True, type=bool))
        self.showticks.setChecked(self.parent.settings.value('show_ticks', True, type=bool))
        self.showcircle1.setChecked(self.parent.settings.value('show_circle1', True, type=bool))
        self.showcircle2.setChecked(self.parent.settings.value('show_circle2', True, type=bool))
        self.showcircle3.setChecked(self.parent.settings.value('show_circle3', True, type=bool))
        self.showticktxt.setChecked(self.parent.settings.value('show_ticktxt', True, type=bool))
        self.showcircle1txt.setChecked(self.parent.settings.value('show_circle1txt', True, type=bool))
        self.showcircle2txt.setChecked(self.parent.settings.value('show_circle1txt', True, type=bool))
        self.showcircle3txt.setChecked(self.parent.settings.value('show_circle1txt', True, type=bool))
        self.offx.setValue(int(self.parent.settings.value('center_x',0)))
        self.offy.setValue(int(self.parent.settings.value('center_y',0)))
        self.tickspacing.setValue(int(self.parent.settings.value('tick_pixels',25)))
        self.tickscale.setValue(float(self.parent.settings.value('tick_distmm',0.05)))
        self.circle1spacing.setValue(int(self.parent.settings.value('rad_circle1',50)))
        self.circle1scale.setValue(float(self.parent.settings.value('rad_circle1mm',0.1)))
        self.circle2spacing.setValue(int(self.parent.settings.value('rad_circle2',100)))
        self.circle2scale.setValue(float(self.parent.settings.value('rad_circle2mm',0.2)))
        self.circle3spacing.setValue(int(self.parent.settings.value('rad_circle3',150)))
        self.circle3scale.setValue(float(self.parent.settings.value('rad_circle3mm',0.3)))
        self.measpix.setValue(int(self.parent.settings.value('measurements_pix',200)))
        self.measmm.setValue(float(self.parent.settings.value('measurements_mm',0.4)))

    def on_close(self):
        self.close()

class Overlay(qtw.QWidget):
    def __init__(self, parent, applied=None):                    
        super().__init__()
        # handle exit condition:
        self.parent = parent
        parent.destroyed.connect(self.close)
        self.startpos = qtc.QPoint(100,100)
        self.endpos = qtc.QPoint(100,100)
        self.is_measuring = False
        self.show()

    def rotate(self, origin, point, angle):
        ox, oy = origin
        px, py = point
        qx = ox + cos(angle) * (px - ox) - sin(angle) * (py - oy)
        qy = oy + sin(angle) * (px - ox) + cos(angle) * (py - oy)
        return qtc.QPoint(int(qx), int(qy))
        
    def mouseDoubleClickEvent(self,event):
        if (event.button() == qtc.Qt.LeftButton):
            self.last = "Double Click"

    def mousePressEvent(self, event):
           self.last = "Click"

    def mouseReleaseEvent(self, event):
        if self.is_measuring == False:
            self.startpos = event.pos() 
        if event.button() == qtc.Qt.LeftButton:
            if self.last == "Click":
                # perform single-click action:
                qtc.QTimer.singleShot(app.instance().doubleClickInterval(),self.performSingleLeftClickAction)
            else:
                # perform double-click action:
                self.setMouseTracking(True)
                self.is_measuring = True
                self.endpos = event.pos()
                self.update()
        elif (event.button() == qtc.Qt.RightButton):
            if len(self.parent.markers) > 0:
                self.parent.markers.pop()

    def performSingleLeftClickAction(self):
        if self.last == "Click":
            if self.is_measuring == True:
                self.is_measuring = False
                self.setMouseTracking(False)
                self.parent.markers.append((self.startpos,self.endpos))
            else:
                self.parent.markers.append(self.startpos)
            self.update()

    def mouseMoveEvent(self, event):
        if self.is_measuring:
            self.endpos = event.pos()
            self.update()
        
    def paintEvent(self, event):
        qp = qtg.QPainter()
        qp.begin(self)
        if self.parent.showsplash == True:
            self.showsplash(qp)
        if len(self.parent.markers) > 0:
            for i,coord in enumerate(self.parent.markers):
                try:
                    self.drawUserMarker(qp,coord.x(),coord.y())
                except:
                    self.drawUserLine(qp,coord[0],coord[1])
        if self.parent.show_dot:
            self.drawPoint(qp)
        if self.parent.show_crosshair:
            self.drawCrosshair(qp)
        if self.parent.show_ticks:
            self.drawTicks(qp, self.parent.tick_pixels, self.parent.tick_distmm, self.parent.show_ticktxt)
        if self.parent.show_circle1:
            self.drawCircle(qp, self.parent.rad_circle1, self.parent.rad_circle1mm, self.parent.show_circle1txt)
        if self.parent.show_circle2:
            self.drawCircle(qp, self.parent.rad_circle2, self.parent.rad_circle2mm, self.parent.show_circle2txt)
        if self.parent.show_circle3:
            self.drawCircle(qp, self.parent.rad_circle3, self.parent.rad_circle3mm, self.parent.show_circle3txt)
        if self.parent.show_center:
            self.drawMarker(qp,int(self.parent.width()/2),int(self.parent.height()/2))
        if self.endpos and self.is_measuring:
            self.measure(qp)
        qp.end()

    def showsplash(self, qp):
        pen = qp.pen()
        pen.setBrush(self.parent.pen_color2)
        qp.setPen(pen)
        rect1 = qtc.QRect(20,20,300,30)
        rect2 = qtc.QRect(20,40,300,30)
        rect3 = qtc.QRect(20,80,300,30)
        rect4 = qtc.QRect(20,100,300,30)
        rect5 = qtc.QRect(20,120,300,30)
        rect6 = qtc.QRect(20,140,300,30)
        rect7 = qtc.QRect(20,160,300,30)
        rect8 = qtc.QRect(20,180,300,30)
        qp.drawText(rect1, qtc.Qt.AlignLeft, '(c)2022 Nils Trapp @ SMoCC/ETH Zürich')
        qp.drawText(rect2, qtc.Qt.AlignLeft, 'https://gitlab.ethz.ch/trappn/video') 
        qp.drawText(rect3, qtc.Qt.AlignLeft, 'CTRL+S: Settings') 
        qp.drawText(rect4, qtc.Qt.AlignLeft, 'CTRL+R: Revert size')
        qp.drawText(rect5, qtc.Qt.AlignLeft, 'LeftClick: Place marker/end measurement') 
        qp.drawText(rect6, qtc.Qt.AlignLeft, 'DoubleClick: Start measurement') 
        qp.drawText(rect7, qtc.Qt.AlignLeft, 'RightClick: Remove last marker')   
        qp.drawText(rect8, qtc.Qt.AlignLeft, 'Space: Remove all markers')

    def measure(self, qp):
        pen = qp.pen()
        pen.setBrush(self.parent.pen_color2)
        pen.setWidth(self.parent.pen_pixels)
        qp.setPen(pen)
        qp.drawLine(self.startpos.x(), self.startpos.y(), self.endpos.x(), self.endpos.y())
        angle = atan2(self.endpos.y()-self.startpos.y(), self.endpos.x()-self.startpos.x())
        lineend1_1 = self.rotate((self.endpos.x(),self.endpos.y()),(self.endpos.x(),self.endpos.y()+5),angle) 
        lineend1_2 = self.rotate((self.endpos.x(),self.endpos.y()),(self.endpos.x(),self.endpos.y()-5),angle)
        lineend2_1 = self.rotate((self.startpos.x(),self.startpos.y()),(self.startpos.x(),self.startpos.y()+5),angle) 
        lineend2_2 = self.rotate((self.startpos.x(),self.startpos.y()),(self.startpos.x(),self.startpos.y()-5),angle)
        qp.drawLine(lineend1_1,lineend1_2)
        qp.drawLine(lineend2_1,lineend2_2)
        dist = round(((self.endpos.x()-self.startpos.x())**2+(self.endpos.y()-self.startpos.y())**2)**0.5)
        txtx = qtc.QRect(self.startpos,self.endpos).center().x()
        txty = qtc.QRect(self.startpos,self.endpos).center().y()
        rect1 = qtc.QRect(txtx+10,txty+10,100,30)
        rect2 = qtc.QRect(txtx+10,txty+30,100,30)
        qp.drawText(rect1, qtc.Qt.AlignLeft, str(dist)+'pix')        
        try:
            distmm = self.parent.measmm/self.parent.measpix*dist
            distmmf = "{:.2f}".format(distmm)
            qp.drawText(rect2, qtc.Qt.AlignLeft, str(distmmf)+'mm')
        except:
            pass
 
    def drawPoint(self, qp):
        pen = qp.pen()
        pen.setBrush(self.parent.pen_color1)
        pen.setWidth(2*self.parent.pen_pixels)
        qp.setPen(pen)
        size = self.parent.size()
        if size.height() <= 1 or size.height() <= 1:
            return
        # scaling for zoom:
        if self.parent.width()/self.parent.camwidth > self.parent.height()/self.parent.camheight:
            scx = int(self.parent.center_x*self.parent.width()/self.parent.camwidth)
            scy = int(self.parent.center_y*self.parent.width()/self.parent.camwidth)
        else:
            scx = int(self.parent.center_x*self.parent.height()/self.parent.camheight)
            scy = int(self.parent.center_y*self.parent.height()/self.parent.camheight)
        x = int(self.parent.width()/2)
        y = int(self.parent.height()/2)
        # draw:
        qp.drawPoint(x+scx, y+scy)

    def drawMarker(self, qp, x, y):
        pen = qp.pen()
        pen.setBrush(self.parent.pen_color2)
        pen.setWidth(self.parent.pen_pixels)
        qp.setPen(pen)
        size = self.parent.size()
        if size.height() <= 1 or size.height() <= 1:
            return
        qp.drawLine(x-5, y, x-20, y)
        qp.drawLine(x, y-5, x, y-20)
        qp.drawLine(x+5, y, x+20, y)
        qp.drawLine(x, y+5, x, y+20)

    def drawUserMarker(self, qp, x, y):
        pen = qp.pen()
        pen.setBrush(self.parent.pen_color2)
        pen.setWidth(self.parent.pen_pixels)
        qp.setPen(pen)
        size = self.parent.size()
        if size.height() <= 1 or size.height() <= 1:
            return
        qp.drawLine(x-3, y-3, x-14, y-14)
        qp.drawLine(x-3, y+3, x-14, y+14)
        qp.drawLine(x+3, y-3, x+14, y-14)
        qp.drawLine(x+3, y+3, x+14, y+14)

    def drawUserLine(self, qp, p1, p2):
        pen = qp.pen()
        pen.setBrush(self.parent.pen_color2)
        pen.setWidth(self.parent.pen_pixels)
        qp.setPen(pen)
        size = self.parent.size()
        if size.height() <= 1 or size.height() <= 1:
            return
        qp.drawLine(p1, p2)
        angle = atan2(p2.y()-p1.y(), p2.x()-p1.x())
        lineend1_1 = self.rotate((p2.x(),p2.y()),(p2.x(),p2.y()+5),angle) 
        lineend1_2 = self.rotate((p2.x(),p2.y()),(p2.x(),p2.y()-5),angle) 
        lineend2_1 = self.rotate((p1.x(),p1.y()),(p1.x(),p1.y()+5),angle) 
        lineend2_2 = self.rotate((p1.x(),p1.y()),(p1.x(),p1.y()-5),angle) 
        qp.drawLine(lineend1_1,lineend1_2)
        qp.drawLine(lineend2_1,lineend2_2)
        dist = round(((p2.x()-p1.x())**2+(p2.y()-p1.y())**2)**0.5)
        txtx = qtc.QRect(p1,p2).center().x()
        txty = qtc.QRect(p1,p2).center().y()
        rect1 = qtc.QRect(txtx+10,txty+10,100,30)
        rect2 = qtc.QRect(txtx+10,txty+30,100,30)
        qp.drawText(rect1, qtc.Qt.AlignLeft, str(dist)+'pix')        
        try:
            distmm = self.parent.measmm/self.parent.measpix*dist
            distmmf = "{:.2f}".format(distmm)
            qp.drawText(rect2, qtc.Qt.AlignLeft, str(distmmf)+'mm')
        except:
            pass

    def drawCrosshair(self, qp):
        pen = qp.pen()
        pen.setBrush(self.parent.pen_color1)
        pen.setWidth(self.parent.pen_pixels)
        qp.setPen(pen)
        size = self.parent.size()
        if size.height() <= 1 or size.height() <= 1:
            return
        # scaling for zoom:
        if self.parent.width()/self.parent.camwidth > self.parent.height()/self.parent.camheight:
            scx = int(self.parent.center_x*self.parent.width()/self.parent.camwidth)
            scy = int(self.parent.center_y*self.parent.width()/self.parent.camwidth)
        else:
            scx = int(self.parent.center_x*self.parent.height()/self.parent.camheight)
            scy = int(self.parent.center_y*self.parent.height()/self.parent.camheight)
        x = int(self.parent.width()/2)
        y = int(self.parent.height()/2)
        qp.drawLine(x+scx-10, y+scy, 20, y+scy)
        qp.drawLine(x+scx, y+scy-10, x+scx, 20)
        qp.drawLine(x+scx+10, y+scy, self.parent.width()-20, y+scy)
        qp.drawLine(x+scx, y+scy+10, x+scx, self.parent.height()-20)

    def drawTicks(self, qp, dist, label, show):
        pen = qp.pen()
        pen.setBrush(self.parent.pen_color1)
        pen.setWidth(self.parent.pen_pixels)
        qp.setPen(pen)
        size = self.parent.size()
        if size.height() <= 1 or size.height() <= 1:
            return
        # scaling for zoom:
        if self.parent.width()/self.parent.camwidth > self.parent.height()/self.parent.camheight:
            scx = int(self.parent.center_x*self.parent.width()/self.parent.camwidth)
            scy = int(self.parent.center_y*self.parent.width()/self.parent.camwidth)
            scd = int(dist*self.parent.width()/self.parent.camwidth)
            scr1 = int(self.parent.rad_circle1*self.parent.width()/self.parent.camwidth)
            scr2 = int(self.parent.rad_circle2*self.parent.width()/self.parent.camwidth)
            scr3 = int(self.parent.rad_circle3*self.parent.width()/self.parent.camwidth)
        else:
            scx = int(self.parent.center_x*self.parent.height()/self.parent.camheight)
            scy = int(self.parent.center_y*self.parent.height()/self.parent.camheight)
            scd = int(dist*self.parent.height()/self.parent.camheight)
            scr1 = int(self.parent.rad_circle1*self.parent.height()/self.parent.camheight)
            scr2 = int(self.parent.rad_circle2*self.parent.height()/self.parent.camheight)
            scr3 = int(self.parent.rad_circle3*self.parent.height()/self.parent.camheight)
        x = int(self.parent.width()/2)
        y = int(self.parent.height()/2)
        i = 1
        # vertical ticks:
        while i*scd < self.parent.height()-y-scy-30:
            if  ((scr1-5 <= i*scd <= scr1+5 and self.parent.show_circle1) or
                (scr2-5 <= i*scd <= scr2+5 and self.parent.show_circle2) or
                (scr3-5 <= i*scd <= scr3+5 and self.parent.show_circle3)):
                # this excludes ticks that coincide with circles
                pass
            else:
                qp.drawLine(x+scx-5, i*scd+y+scy, x+scx+5, i*scd+y+scy)
            i += 1
        i = 1
        while -i*scd+y+scy > 30:
            if  ((scr1-5 <= i*scd <= scr1+5 and self.parent.show_circle1) or
                (scr2-5 <= i*scd <= scr2+5 and self.parent.show_circle2) or
                (scr3-5 <= i*scd <= scr3+5 and self.parent.show_circle3)):
                # this excludes ticks that coincide with circles
                pass
            else:
                qp.drawLine(x+scx-5, -i*scd+y+scy, x+scx+5, -i*scd+y+scy)
            i += 1
        # horizontal ticks:
        i = 1
        while i*scd < self.parent.width()-x-scx-30:
            if  ((scr1-5 <= i*scd <= scr1+5 and self.parent.show_circle1) or
                (scr2-5 <= i*scd <= scr2+5 and self.parent.show_circle2) or
                (scr3-5 <= i*scd <= scr3+5 and self.parent.show_circle3)):
                # this excludes ticks that coincide with circles
                pass 
            else:
                qp.drawLine(i*scd+x+scx, y+scy-5, i*scd+x+scx, y+scy+5)
            i += 1
        # legend:
        if show == True:
            rect = qtc.QRect((i-3)*scd+x+scx-2, 2*y-40, 200, 200)
            labelf = "{:.2f}".format(label)
            qp.drawLine((i-3)*scd+x+scx, 2*y-50, (i-3)*scd+x+scx, 2*y-40)
            qp.drawLine((i-2)*scd+x+scx, 2*y-50, (i-2)*scd+x+scx, 2*y-40)
            qp.drawLine((i-3)*scd+x+scx, 2*y-45, (i-2)*scd+x+scx, 2*y-45)
            qp.drawText(rect, qtc.Qt.AlignLeft, str(labelf))
        i = 1
        while -i*scd+x+scx > 30:
            if  ((scr1-5 <= i*scd <= scr1+5 and self.parent.show_circle1) or
                (scr2-5 <= i*scd <= scr2+5 and self.parent.show_circle2) or
                (scr3-5 <= i*scd <= scr3+5 and self.parent.show_circle3)):
                # this excludes ticks that coincide with circles
                pass
            else:
                qp.drawLine(-i*scd+x+scx, y+scy-5, -i*scd+x+scx, y+scy+5)
            i += 1

    def drawCircle(self, qp, radius, label, show):
        pen = qp.pen()
        pen.setBrush(self.parent.pen_color1)
        pen.setWidth(self.parent.pen_pixels)
        qp.setPen(pen)
        size = self.parent.size()
        if size.height() <= 1 or size.height() <= 1:
            return
        x = int(size.width()/2)
        y = int(size.height()/2)
        # scaling for zoom:
        if self.parent.width()/self.parent.camwidth > self.parent.height()/self.parent.camheight:
            scx = int(self.parent.center_x*self.parent.width()/self.parent.camwidth)
            scy = int(self.parent.center_y*self.parent.width()/self.parent.camwidth)
            scr = int(radius*self.parent.width()/self.parent.camwidth)
        else:
            scx = int(self.parent.center_x*self.parent.height()/self.parent.camheight)
            scy = int(self.parent.center_y*self.parent.height()/self.parent.camheight)
            scr = int(radius*self.parent.height()/self.parent.camheight)
        # draw:
        qp.drawEllipse(qtc.QPoint(x+scx, y+scy), scr, scr)
        if show == True:
            rect = qtc.QRect(x+scx+scr+2, y+scy+5, 200, 200)
            labelf = "{:.2f}".format(label)
            qp.drawText(rect, qtc.Qt.AlignLeft, str(labelf))

class MainWindow(qtw.QMainWindow):

    VERSION = 'RC1.0'
    emergency_stop = qtc.pyqtSignal()
    
    def __init__(self):
        """MainWindow constructor.
        """
        super().__init__()
        # kill child processes on exit:
        self.setAttribute(qtc.Qt.WA_DeleteOnClose)
        # Main framework
        self.setWindowTitle('VIDEO.EXE ' + self.VERSION)
        self.settings = qtc.QSettings('SMoCC','video.exe')
        base_widget = qtw.QWidget()
        base_widget.setLayout(qtw.QStackedLayout())
        base_widget.layout().setContentsMargins(0,0,0,0)
        base_widget.layout().setStackingMode(1)
        self.setCentralWidget(base_widget)
        self.available_cameras = qtmm.QCameraInfo.availableCameras()
        # Create the viewfinder:
        self.cvf = qtmmw.QCameraViewfinder()
        self.cvf.setStyleSheet('border:0')
        self.cvf.setAspectRatioMode(2)
        # get camera and other stuff:
        self.select_camera(0)
        self.getSettings()
        # toggle always on top etc.:
        if self.alwaysontop:
            self.setWindowFlags(self.windowFlags() | qtc.Qt.WindowStaysOnTopHint)
        self.camera.setViewfinder(self.cvf)
        base_widget.layout().addWidget(self.cvf)
        if self.use_custom_geometry == True:
            self.resize(self.custom_geometry.width(),self.custom_geometry.height())
        else:
            self.resize(qtc.QSize(self.camwidth, self.camheight))
        self.overlay = Overlay(self)
        base_widget.layout().addWidget(self.overlay)
        base_widget.layout().setCurrentWidget(self.overlay)
        self.markers = list()
        self.showsplash = True
        qtc.QTimer.singleShot(10000,self.hidesplash)
        self.show()

    def keyPressEvent(self, event):
        """Hotkey handling (CTRL+S for settings, CTRL+Q to quit,
        CTRL-R to restore native size).
        """
        if type(event) == qtg.QKeyEvent:
            if event.modifiers() == qtc.Qt.ControlModifier:
                if event.key() == qtc.Qt.Key_S:
                    self.setSettings()
                elif event.key() == qtc.Qt.Key_R:
                    self.restoreSize()
                elif event.key() == qtc.Qt.Key_Q:
                    self.close()
                elif event.key() == qtc.Qt.Key_C:
                    self.close()
            else:
                if event.key() == qtc.Qt.Key_Space:
                    self.markers = list()

    def select_camera(self, i):
        self.camera = qtmm.QCamera(self.available_cameras[i])
        self.camera.setViewfinder(self.cvf)
        self.camera.setCaptureMode(qtmm.QCamera.CaptureStillImage)
        self.camera.start()
        # Configure capture
        self.capture = qtmm.QCameraImageCapture(self.camera)
        self.capture.error.connect(lambda i, e, s: self.return_error(s))
        self.current_camera_name = self.available_cameras[i].description()
        settings = self.capture.encodingSettings()
        supportedResolutions, _ = self.capture.supportedResolutions()
        # just default to highest available resolution for now:
        try:
            self.camwidth = supportedResolutions[-1].width()
            self.camheight = supportedResolutions[-1].height()
        except:
            qtw.QMessageBox.about(self, 'VIDEO.EXE - No Camera', 'Program needs at least one camera to run.')
            self.close()
        settings.setResolution(self.camwidth, self.camheight)
        settings.setQuality(qtmm.QMultimedia.VeryHighQuality)
        self.capture.setEncodingSettings(settings)

    def hidesplash(self):
        self.showsplash = False

    def update_overlay(self):
        self.pen_color1 = self.settingswindow.color1._color
        self.pen_color2 = self.settingswindow.color2._color
        # transparencies:
        self.pen_color1.setAlpha(125)
        self.pen_color2.setAlpha(125)
        self.pen_pixels = self.settingswindow.penpix.value()
        self.center_x = self.settingswindow.offx.value()
        self.center_y = self.settingswindow.offy.value()
        self.tick_pixels = self.settingswindow.tickspacing.value()
        self.tick_distmm = self.settingswindow.tickscale.value()
        self.rad_circle1 = self.settingswindow.circle1spacing.value()
        self.rad_circle1mm = self.settingswindow.circle1scale.value()
        self.rad_circle2 = self.settingswindow.circle2spacing.value()
        self.rad_circle2mm = self.settingswindow.circle2scale.value()
        self.rad_circle3 = self.settingswindow.circle3spacing.value()
        self.rad_circle3mm = self.settingswindow.circle3scale.value()        
        self.show_dot = self.settingswindow.showdot.isChecked()
        self.show_crosshair = self.settingswindow.showcross.isChecked()
        self.show_ticks = self.settingswindow.showticks.isChecked()
        self.show_circle1 = self.settingswindow.showcircle1.isChecked()
        self.show_circle2 = self.settingswindow.showcircle2.isChecked()
        self.show_circle3 = self.settingswindow.showcircle3.isChecked()
        self.show_ticktxt = self.settingswindow.showticktxt.isChecked()
        self.show_circle1txt = self.settingswindow.showcircle1txt.isChecked()
        self.show_circle2txt = self.settingswindow.showcircle2txt.isChecked()
        self.show_circle3txt = self.settingswindow.showcircle3txt.isChecked()
        self.show_center = self.settingswindow.showcamctr.isChecked()
        self.measpix = self.settingswindow.measpix.value()
        self.measmm = self.settingswindow.measmm.value()
        self.overlay.update()

    def update_window_properties(self):
        self.alwaysontop = self.settingswindow.aot.isChecked()
        if self.alwaysontop == True:
            # enable:
            self.setWindowFlags(self.windowFlags() | qtc.Qt.WindowStaysOnTopHint)
            self.show()
            self.settingswindow.activateWindow()
        else:
            # disable:
            self.setWindowFlags(self.windowFlags() & ~qtc.Qt.WindowStaysOnTopHint)
            self.show()
        
    def restoreSize(self):
        if self.use_custom_geometry == True:
            self.resize(self.custom_geometry.width(),self.custom_geometry.height())
        else:
            self.resize(qtc.QSize(self.camwidth, self.camheight))

    def setSettings(self):
        self.settingswindow = SettingsWindow(self)
  
    def getSettings(self):
        """Recover settings, convert from str back to QObj
        where necessary.
        """
        self.pen_color1 = self.settings.value('color1', qtg.QColor('#FFFFFF'))
        self.pen_color2 = self.settings.value('color2', qtg.QColor('#FFFF00'))
        # transparencies:
        self.pen_color1.setAlpha(125)
        self.pen_color2.setAlpha(125)
        self.custom_geometry = self.settings.value('custom_geometry', self.geometry())
        self.use_custom_geometry = self.settings.value('use_custom_geometry', False)
        self.alwaysontop = self.settings.value('alwaysontop', False, type=bool)
        self.show_center = self.settings.value('show_center', False, type=bool)
        self.show_dot = self.settings.value('show_dot', False, type=bool)
        self.show_crosshair = self.settings.value('show_crosshair', True, type=bool)
        self.show_ticks = self.settings.value('show_ticks', True, type=bool)
        self.show_circle1 = self.settings.value('show_circle1', True, type=bool)
        self.show_circle2 = self.settings.value('show_circle2', True, type=bool)
        self.show_circle3 = self.settings.value('show_circle3', True, type=bool)
        self.show_ticktxt = self.settings.value('show_ticktxt', False, type=bool)
        self.show_circle1txt = self.settings.value('show_circle1txt', False, type=bool)
        self.show_circle2txt = self.settings.value('show_circle2txt', False, type=bool)
        self.show_circle3txt = self.settings.value('show_circle3txt', False, type=bool)
        self.center_x = int(self.settings.value('center_x', 0))
        self.center_y = int(self.settings.value('center_y', 0))
        self.tick_pixels = int(self.settings.value('tick_pixels', 25))
        self.rad_circle1 = int(self.settings.value('rad_circle1', 50))
        self.rad_circle2 = int(self.settings.value('rad_circle2', 100))
        self.rad_circle3 = int(self.settings.value('rad_circle3', 150))
        self.tick_distmm = float(self.settings.value('tick_distmm', 0.05))
        self.rad_circle1mm = float(self.settings.value('rad_circle1mm', 0.1))
        self.rad_circle2mm = float(self.settings.value('rad_circle2mm', 0.2))
        self.rad_circle3mm = float(self.settings.value('rad_circle3mm', 0.3))
        self.measpix = int(self.settings.value('measurements_pix', 200))
        self.measmm = float(self.settings.value('measurements_mm', 0.4))
        self.pen_pixels = int(self.settings.value('pen_pixels', 2))

        # retranslate unique camera&port strings back to selection index:
        for i, c in enumerate(self.available_cameras):
            if self.settings.value('camera', 'none') == c.deviceName():
                self.select_camera(i)
    
    def closeEvent(self, event):
        pass


if __name__ == '__main__':
    app = qtw.QApplication(sys.argv)
    app.setDoubleClickInterval(150)
    mw = MainWindow()
    sys.exit(app.exec())
