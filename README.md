# VIDEO.EXE

Simple replacement for Bruker VIDEO.EXE in order to use a USB camera instead of a TV camera with framegrabber. The original
code supports only a handful of framegrabber cards, which are mostly not supported in Windows 7 and up. Also USB cams are cheaper.<br>
Tested successfully with Bruker BIS classic under Windows 10 Pro-32bit. Just create the directory "C:\Program Files\BrukerAXS\Video" and put video.exe there. BIS will find and run it automatically, but I found this interferes with Apex4 (it will try to communicate and keep starting new video.exe instances). My hacky way around this, until I found out how I can make it communicate like the original:<br>
1. Put my video.exe in C:\Program Files\BrukerAXS\Video_NT (where Apex/BIS cannot pick it up) <br>
2. Disable microscope in BCP. <br>
3. Put autorun_video.exe in autostart. This will check every 10 seconds in the background if BIS is running, and start video.exe if this is the case. It will also restart video.exe when you close it.
<br>
I also made a custom version that automatically runs maximized on a second display, which I have inside the enclosure.

Requires: PyQt5, PyQt5-multimedia (and psutil for autorun / standalone)

I take no responsibility whatsoever for any damage arising from use of this software.
<br><br>

Features:
- Fast, no-clutter display
- Hotkeys (CTRL+S, CTRL+Q, CTRL+C, CTRL+R): Settings, Quit, Resize to original camera resolution
- Correct scaling of crosshair/circles on window resize
- User markers: Left click to add, right click to remove last, space to remove all
- Measurements (scaled on a reference object defined in settings): Double left click to start measurement, single left click to stop, right click to remove last, space to remove all
- Many configuration options (see image), settings are persistent


![GUI overview image](/img/video.png "Camera View with Settings")
